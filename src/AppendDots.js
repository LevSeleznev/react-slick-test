import * as React from 'react';
import Slider from 'react-slick';

export default class ScreenshotsSlider extends React.Component {
    constructor(props) {
        super(props);

        this.handleAfterChange = this.handleAfterChange.bind(this);
        this.handleAfterChangeThrumbnail = this.handleAfterChangeThrumbnail.bind(this);
    }

    shouldComponentUpdate() {
        return false;
    }

    handleAfterChange(current) {
        this.thrumbnailSlider.slickGoTo(current);
    }

    handleAfterChangeThrumbnail(current) {
        this.slider.slickGoTo(current);
    }

    render() {
        return (
            <div>
                <Slider
                    ref={slider => this.slider = slider}
                    autoplay={true}
                    afterChange={this.handleAfterChange}
                >
                    { this.props.items.map((item, index) => {
                        return (
                            <div
                                key={index}
                                style={{
                                    marginBottom: '20px'
                                }}
                            >
                                <img
                                    src={item}
                                    alt=''
                                    style={{
                                        height: '300px',
                                        width: '100%',
                                        maxWidth: '100%'
                                    }}
                                />
                            </div>
                        );
                    }) }
                </Slider>
                <Slider
                    ref={slider => this.thrumbnailSlider = slider}
                    slidesToShow={3}
                    swipeToSlide
                    focusOnSelect
                    infinite={false}
                    afterChange={this.handleAfterChangeThrumbnail}
                >
                    { this.props.items.map((item, index) => {
                        return (
                            <div
                                key={index}
                                style={{
                                    height: '100px',
                                    backgroundImage: `url(${item})`,
                                    backgroundRepeat: 'no-repeat',
                                    backgroundPosition: 'center',
                                    backgroundSize: 'cover',
                                    overflow: 'hidden'
                                }}
                                afterChange={this.props.afterChange}
                            />
                        );
                    }) }
                </Slider>
            </div>
        );
    }
}
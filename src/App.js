import React, { Component } from 'react';
import AppendDots from './AppendDots';

class App extends Component {
  render() {
    return (
      <div className="page">
          <div className="page-body" style={{
              width: '60%',
              margin: '20px auto',
              overflow: 'hidden'
          }}>
              <AppendDots
                items={[
                    'images/seattle_1.jpg',
                    'images/seattle_2.jpg',
                    'images/seattle_3.jpg',
                    'images/seattle_4.jpg',
                    'images/seattle_5.jpg',
                    'images/seattle_6.jpg',
                ]}
              />
          </div>
      </div>
    );
  }
}

export default App;
